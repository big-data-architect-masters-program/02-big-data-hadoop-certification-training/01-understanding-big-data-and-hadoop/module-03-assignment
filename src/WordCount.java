import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.StringTokenizer;

public class WordCount {
    public class WordCountMapper extends Mapper<LongWritable, Text,Text, IntWritable>{

        Text word = new Text();
        IntWritable one = new IntWritable(1);
        public void map(LongWritable key, Text value, Mapper.Context con)throws IOException, InterruptedException{

            // Business logic
            StringTokenizer itr = new StringTokenizer(value.toString());

            while(itr.hasMoreElements()){
                word.set(itr.nextToken());
                con.write(word, one);
            }
        }

    }

    public class WordCountReducer extends Reducer<Text,IntWritable,Text,IntWritable> {

        IntWritable  result = new IntWritable();

        public void reduce(Text key, Iterable<IntWritable> values, Context con)throws IOException, InterruptedException{

            //Aggregation or Consolidation
            int sum = 0;
            for(IntWritable val : values ){
                sum += val.get();
            }
            result.set(sum);
            con.write(key, result);

        }

    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "MyFirstWordCount");

        job.setJarByClass(WordCount.class);
        job.setMapperClass(WordCountMapper.class);
        job.setReducerClass(WordCountReducer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.waitForCompletion(true);
    }

}
